import crowdai
import argparse
from sklearn.svm import SVR
import numpy as np
import danutils as du
import pandas as pd
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import RandomizedSearchCV
def mean_absolute_percentage_error(y_true, y_pred):
    if any(y_true == 0):
        return 0.0
    else:
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100
import time

submit = False

## Fix random seed for reproducibility
seed = 7
np.random.seed(seed)

#Load training data
#x_train = np.load("data/subset_cm_train.npy")

X = du.load("data/genotyping_data_subset_train.vcf")
y = np.load("data/train_heights.npy")

#X_test = du.load("data/genotyping_data_subset_test.vcf")

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=seed)

print X_train.shape
print X_train

## General and CV options
n_iter = 80
n_jobs = -1
cv = 5
max_iter = 1000
batch_size = 32

## Parameters for MLP
neurons_output = max(1, y_train.shape[1] if len(y_train.shape) > 1 else 1)
minNeuron = neurons_output
maxNeuron = X_train.shape[1] / 2 #int(X_train.shape[0] / (2.0 * (X_train.shape[1] + neurons_output)))

print "Min neurons" + str(minNeuron)
print "Max neurons" + str(maxNeuron)

neurons_size = [10, 100, 1000, 4000]

## MLPRegressor
MLPR_mdl = MLPRegressor(random_state=seed, max_iter=max_iter, batch_size=batch_size)
MLPR_param_distributions = {
        "hidden_layer_sizes": [(x,) for x in neurons_size],
        "solver": ["lbfgs", "sgd", "adam"],
        "learning_rate": ["constant", "invscaling", "adaptive"],
        "activation": ["identity", "logistic", "tanh", "relu"],
        "alpha": list(np.random.uniform(low=0, high=5, size=n_iter)),
        "learning_rate_init": list(10.0 ** np.random.uniform(low=-6, high=-2, size=n_iter)),
        "momentum": list(np.random.uniform(low=1E-2, high=1, size=n_iter)),
        "nesterovs_momentum": [True, False],
        "beta_1": list(np.random.uniform(low=1E-2, high=1, size=n_iter)),
        "beta_2": list(np.random.uniform(low=1E-2, high=1, size=n_iter))}
    
MLPR_random_search = RandomizedSearchCV(estimator=MLPR_mdl, cv=cv, n_iter=n_iter, param_distributions=MLPR_param_distributions, n_jobs=n_jobs, verbose=1)
print "Starting randomize search"
MLPR_start = time.time()
MLPR_result = MLPR_random_search.fit(X_train, y_train)
MLPR_end = time.time()
print "Finished randomized search, predicting..."
MLPR_y_pred = MLPR_result.predict(X=X_test)
print "Checking results"

## Summarize results
print("MLPRegressor")
print("============")
print("Time: {0}".format(MLPR_end - MLPR_start))
print("Score: {0}".format(MLPR_result.best_score_))
print("Parameters: {0}".format(MLPR_result.best_params_))
print("r2_score: {0}".format(sklearn.metrics.r2_score(y_true=y_test, y_pred=MLPR_y_pred)))
print("mean_absolute_error: {0}".format(sklearn.metrics.mean_absolute_error(y_true=y_test, y_pred=MLPR_y_pred)))
print("mean_squared_error: {0}".format(sklearn.metrics.mean_squared_error(y_true=y_test, y_pred=MLPR_y_pred)))
print("median_absolute_error: {0}".format(sklearn.metrics.median_absolute_error(y_true=y_test, y_pred=MLPR_y_pred)))
print("mean_absolute_percentage_error: {0}".format(mean_absolute_percentage_error(y_true=y_test, y_pred=MLPR_y_pred)))
print()

#scores = cross_val_score(nn, x_train, y_train, cv=5)
#print scores
#print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

#nn.fit(x_train, y_train)
# Predict the heights for the test set
heights = nn.predict(x_test)
print heights

print scores.mean()

if(scores.mean() > 0.4):
    print "boum"
    
heights = heights.tolist()

# Create the challenge object by authentication with crowdAI with your API_KEY
if(submit):
    challenge = crowdai.Challenge("OpenSNPChallenge2017", "11a3ae7ba30fbfef150ac36b3f88da9a")
    challenge.submit(heights)
    challenge.disconnect()