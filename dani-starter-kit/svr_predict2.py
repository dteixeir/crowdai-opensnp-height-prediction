import crowdai
import argparse
from sklearn.svm import SVR
from sklearn.svm import NuSVR
import numpy as np
from sklearn.model_selection import cross_val_score

#Load training data
x_train = np.load("data/subset_cm_train.npy")
y_train = np.load("data/train_heights.npy")

x_test = np.load("data/subset_cm_test.npy")

#Replace nan values in the training and testing set with an arbitrary number
inds = np.where(np.isnan(x_train))
x_train[inds] = -100
inds = np.where(np.isnan(x_test))
x_test[inds] = -100



nn = MLPRegressor(
    hidden_layer_sizes=(10,),  activation='relu', solver='adam', alpha=0.001, batch_size='auto',
    learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True,
    random_state=9, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,
    early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

n = nn.fit(x_train, y_train)
# Predict the heights for the test set
heights = nn.predict(x_test)

#Convert heights from np.array to a list (to ensure it is JSON serializable)
heights = heights.tolist()

print heights