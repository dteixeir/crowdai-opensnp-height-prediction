import crowdai
import argparse
from sklearn.svm import SVR
import numpy as np
from sklearn.model_selection import cross_val_score

#Load training data
x_train = np.load("data/subset_cm_train.npy")
y_train = np.load("data/train_heights.npy")

x_test = np.load("data/subset_cm_test.npy")

#Replace nan values in the training and testing set with an arbitrary number
inds = np.where(np.isnan(x_train))
x_train[inds] = -100
inds = np.where(np.isnan(x_test))
x_test[inds] = -100


# Instantiate a linear model
clf = SVR(kernel = 'poly', C=10, epsilon=0.2)
scores = cross_val_score(clf, x_train, y_train, cv=5)

print scores

clf.fit(x_train, y_train)

# Predict the heights for the test set
heights = clf.predict(x_test)

#Convert heights from np.array to a list (to ensure it is JSON serializable)
heights = heights.tolist()

print heights