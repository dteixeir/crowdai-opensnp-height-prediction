import crowdai
import argparse
from sklearn.svm import SVR
import numpy as np
import danutils as du
import pandas as pd
from sklearn.model_selection import GridSearchCV, cross_val_score

submit = False

# Create the challenge object by authentication with crowdAI with your API_KEY
if(submit):
    challenge = crowdai.Challenge("OpenSNPChallenge2017", "11a3ae7ba30fbfef150ac36b3f88da9a")

x_train= du.convertVCFToMatrix("data/genotyping_data_subset_train.vcf")
y_train = np.load("data/train_heights.npy")

clf = SVR(C=1.0, epsilon=0.2)
scores = cross_val_score(clf, x_train, y_train, cv=5)
clf.fit(x_train, y_train)
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

x_test = du.convertVCFToMatrix("data/genotyping_data_subset_test.vcf")
clf.predict(x_test)

if(submit):
    challenge.submit(heights)
    challenge.disconnect()