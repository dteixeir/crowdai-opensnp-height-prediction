import argparse
from sklearn.svm import SVR
import numpy as np
import danutils as du
import pandas as pd
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error, median_absolute_error
 
def mean_absolute_percentage_error(y_true, y_pred):
    if any(y_true == 0):
        return 0.0
    else:
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100
import time

submit = False

## Fix random seed for reproducibility
seed = 3
np.random.seed(seed)

#Load training data
X = np.load("data/subset_cm_train.npy")
y = np.load("data/train_heights.npy")

inds = np.where(np.isnan(X))
X[inds] = -100

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=seed)

## General and CV options
n_iter = 10
n_jobs = 40
cv = 5
max_iter = 100
batch_size = 5

for i in range (1, 3):
    X_train = np.concatenate((X_train, X_train), axis=0)
    y_train = np.concatenate((y_train, y_train + 0.01), axis=0)


# See descriptions: http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html
## MLPRegressor
MLPR_mdl = MLPRegressor(random_state=seed, max_iter=max_iter, batch_size=batch_size, shuffle=True, early_stopping=True)
MLPR_param_distributions = {
        "hidden_layer_sizes": [12, 15, 17, 20, 22, 25, 27],
        "solver": ["lbfgs", "adam"],
        "learning_rate": ["constant"],
        "tol": [0.0001, 0.001, 0.01],
        "activation": ["relu", "identity"],
        "alpha": [0.0001, 0.001, 0.01, 0.1, 1],
        "learning_rate_init": [0.01, 0.1, 1]}
    
MLPR_grid_search = GridSearchCV(estimator=MLPR_mdl, cv=cv, param_grid=MLPR_param_distributions, n_jobs=n_jobs, verbose=1)
print "Starting randomize search"
MLPR_start = time.time()
MLPR_result = MLPR_grid_search.fit(X_train, y_train)
MLPR_end = time.time()
print "Finished randomized search, predicting..."
MLPR_y_pred = MLPR_result.predict(X=X_test)
print "Checking results"

## Summarize results
print("MLPRegressor")
print("============")
print("Time: {0}".format(MLPR_end - MLPR_start))
print("Score: {0}".format(MLPR_result.best_score_))
print("Parameters: {0}".format(MLPR_result.best_params_))
print("r2_score: {0}".format(r2_score(y_true=y_test, y_pred=MLPR_y_pred)))
print("mean_absolute_error: {0}".format(mean_absolute_error(y_true=y_test, y_pred=MLPR_y_pred)))
print("mean_squared_error: {0}".format(mean_squared_error(y_true=y_test, y_pred=MLPR_y_pred)))
print("median_absolute_error: {0}".format(median_absolute_error(y_true=y_test, y_pred=MLPR_y_pred)))
print("mean_absolute_percentage_error: {0}".format(mean_absolute_percentage_error(y_true=y_test, y_pred=MLPR_y_pred)))
print()