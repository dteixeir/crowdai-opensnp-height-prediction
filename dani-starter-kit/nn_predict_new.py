import argparse
from sklearn.svm import SVR
import numpy as np
import danutils as du
import pandas as pd
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.neural_network import MLPRegressor

submit = False

# Create the challenge object by authentication with crowdAI with your API_KEY
#if(submit):
#    challenge = crowdai.Challenge("OpenSNPChallenge2017", "11a3ae7ba30fbfef150ac36b3f88da9a")

#Load training data
x_train = np.load("data/subset_cm_train.npy")

y_train = np.load("data/train_heights.npy")

x_test = np.load("data/subset_cm_test.npy")

#Replace nan values in the training and testing set with an arbitrary number
inds = np.where(np.isnan(x_train))
x_train[inds] = -100
inds = np.where(np.isnan(x_test))
x_test[inds] = -100

nn = MLPRegressor(
    hidden_layer_sizes=(30),  activation='relu', solver='lbfgs', alpha=0.001, batch_size='auto',
    learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True,
    random_state=9, tol=0.0001, verbose=False, warm_start=True, momentum=0.9, nesterovs_momentum=True,
    early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

scores = cross_val_score(nn, x_train, y_train, cv=5)
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

nn.fit(x_train, y_train)
# Predict the heights for the test set
heights = nn.predict(x_test)

heights = heights.tolist()

print heights

if(submit):
    challenge.submit(heights)
    challenge.disconnect()
