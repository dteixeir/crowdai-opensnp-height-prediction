import numpy as np
import csv

def addSexToMatrix( matrix ):
    #Corresponds to number of patients, example 784
    rows = matrix.shape[0] 
    #Corresponds to number of variants, example 9894
    cols = matrix.shape[1] 
    arrayWithSex = np.zeros((rows, cols + 1), dtype=np.float)
    i = 0
    for row in arrayWithSex:
        #sex for male (devil)
        sex = MALE
        if np.isnan(matrix[i][cols - 1]) and np.isnan(matrix[i][cols - 2]) and np.isnan(matrix[i][cols - 3]):
            sex = FEMALE 
        arrayWithSex[i] = np.append(sex, matrix[i]) 
        i = i + 1
    return arrayWithSex

def get_sex_for_training(sex_number, x_train, y_train):
    sex = []
    ys = []
    i = 0
    for row in x_train:
        if(row[0] == sex_number):
            sex.append(x_train[i]) 
            ys.append(y_train[i])
        i = i + 1
    return {'x': sex, 'y': ys}
            
def cntAlleles(x):
    x = x.replace("\n", "")
    if(x == "0/0"):
        return 0
    elif(x == "0/1"):
        return 0.5
    elif(x == "1/0"):
        return 0.5
    elif(x == "1/1"):
        return 1.0
    elif(x == "./."):
        return -1.0
    else:
        return x
        

def saveToCsv(fileName, matrix):
    with open(fileName, "wb") as f:
        writer = csv.writer(f)
        writer.writerows(matrix)

def saveHeights(fileName = "data/train_heights.npy"):
    y_train = np.load(fileName)
    i = 1
    patientHeigths = []
    patientHeigths.append(["patientId", "height"])
    for y in y_train:
        patientHeigths.append([i, y])
        i = i + 1
    saveToCsv("patient-heights.csv", patientHeigths)

def loadVariantsDict(variantFile): 
    firstLine = True
    vardict = {}
    with open(variantFile, "r") as vf:
        for var in vf:
            if(firstLine):
                firstLine = False
                continue
            elms = var.split("\t")
            e = {"chromosome": elms[0], "pos": elms[1], "variantId": elms[2],  "type": elms[7], "intensity": elms[8], "gene": elms[9]}
            vardict[elms[2]] = e
    return vardict
    

#According to this publication: https://www.nature.com/nature/journal/v542/n7640/full/nature21039.html    
GENES = ["IHH", "STC2", "AR", "CRISPLD2", "STC2", "IGFBP", "PAPP", "ADAMTS3", "IL11RA", "NOX4"]

def load(vcfFile):
    variantDict = loadVariantsDict("subset-data/variant-details.csv")
    patients = []
    array = []
    with open(vcfFile, "r") as f:
        for line in f:
            if(not line.startswith("#")):
                elms = line.split("\t")
                array.append(map(cntAlleles, elms))
    patientsCount = len(array[0]) - 9
    
    ##Add the headers
    patientHeaders = []
    patientHeaders.append("patientId")
    patientHeaders.append("male")
    for row in array:
        patientHeaders.append(row[2])
    
    #patients.append(["patientId", "male"] + GENES) 

    ##Pivot the data
    for patientId in range(1, patientsCount + 1):
        #Suppose it's a male
        sex = 0
        variants = []
        genesCount = {
            "IHH" : 0, 
            "STC2" : 0, 
            "AR" : 0, 
            "CRISPLD2" : 0,
            "STC2" : 0, 
            "IGFBP" : 0, 
            "PAPP": 0, 
            "ADAMTS3": 0, 
            "IL11RA": 0, 
            "NOX4": 0}
        
        for rowIdx in range(0, len(array)):
            rsVar = patientHeaders[rowIdx]
            row = array[rowIdx]
            numberofAlleles = row[patientId + 8]
            #if rsVar in variantDict:
            #    varInfo = variantDict[rsVar]
                #for gene in GENES:
                #    if(varInfo["gene"].lower() in gene.lower()):
                #        currentCount = genesCount[gene]
                #        if(numberofAlleles > 0):
                #            genesCount[gene] = currentCount + numberofAlleles
            if(sex == 0 and row[0] == '24'):
                if(row[patientId + 8] >= 0):
                    sex = 1.0
            variants.append(numberofAlleles)
            #print genesCount
            #print genesCount.values()
            #genesCountArray = genesCount.values()
            #print genesCountArray
        pv = [sex] + variants
        #pv = [patientId, sex] + genesCountArray
        patients.append(pv)
    return np.array(patients)
