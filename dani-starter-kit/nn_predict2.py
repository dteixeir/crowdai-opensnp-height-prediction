import crowdai
import argparse
from sklearn.svm import SVR
import numpy as np
import danutils as du
import pandas as pd
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler 

submit = False

#Load training data

y_train = np.load("data/train_heights.npy")
x_train = du.load("data/genotyping_data_subset_train.vcf")
x_test = du.load("data/genotyping_data_subset_test.vcf")

nn = MLPRegressor(
        hidden_layer_sizes=(100,),  activation='relu', solver='lbfgs', alpha=0.001, batch_size='auto',
        learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True,
        random_state=9, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,
        early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

scores = cross_val_score(nn, x_train, y_train, cv=5)

print scores
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

nn.fit(x_train, y_train)
# Predict the heights for the test set
heights = nn.predict(x_test)
print heights

heights = heights.tolist()

# Create the challenge object by authentication with crowdAI with your API_KEY
if(submit):
    challenge = crowdai.Challenge("OpenSNPChallenge2017", "11a3ae7ba30fbfef150ac36b3f88da9a")
    challenge.submit(heights)
    challenge.disconnect()
