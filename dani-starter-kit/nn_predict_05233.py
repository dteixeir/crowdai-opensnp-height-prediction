import crowdai
import argparse
from sklearn.svm import SVR
import numpy as np
import danutils as du
import pandas as pd
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error, median_absolute_error

submit = True
seed = 9

# Create the challenge object by authentication with crowdAI with your API_KEY
if(submit):
    challenge = crowdai.Challenge("OpenSNPChallenge2017", "YOUR_KEY")

#Load training data
x_train = np.load("data/subset_cm_train.npy")
x_train = np.concatenate((x_train, np.load("data/p0_train.npy")), axis=1)

y_train = np.load("data/train_heights.npy")

x_test = np.load("data/subset_cm_test.npy")
x_test = np.concatenate((x_test, np.load("data/p0_test.npy")), axis=1)

#Replace nan values in the training and testing set with an arbitrary number
inds = np.where(np.isnan(x_train))
x_train[inds] = -100
inds = np.where(np.isnan(x_test))
x_test[inds] = -100

#from sklearn.model_selection import train_test_split
#x_train, x_test, y_train, y_test = train_test_split(x_train, y_train, test_size=0.1, random_state=seed)

nn = MLPRegressor(
    hidden_layer_sizes=(200,),  activation='relu', solver='lbfgs', alpha=0.001, batch_size=1,
    learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True,
    random_state=seed, tol=0.0001, verbose=False, warm_start=True, momentum=0.9, nesterovs_momentum=True,
    early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)


nn.fit(x_train, y_train)

#y_pred=nn.predict(x_test)
#print("r2_score: {0}".format(r2_score(y_true=y_test, y_pred=y_pred)))
#print("mean_squared_error: {0}".format(mean_squared_error(y_true=y_test, y_pred=y_pred)))

#quit()


# Predict the heights for the test set
heights = nn.predict(x_test)

print heights

heights = heights.tolist()

if(submit):
    challenge.submit(heights)
    challenge.disconnect()
